package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class DBManager {

	private static DBManager instance;

	Connection connection;

	public static synchronized DBManager getInstance() {
		if(instance == null){
			try {
				instance = new DBManager();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return instance;
	}

	private DBManager() throws IOException {
		Properties properties = loadProperties();
		String url = properties.getProperty("connection.url");

		try {
			connection = DriverManager.getConnection(url);

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		ResultSet resultSet;

		if (connection != null) {
			try {

				Statement statement = connection.createStatement();
				resultSet = statement.executeQuery("SELECT * FROM users");
				User user;
				while (resultSet.next()) {
					user = new User();
					user.setId(resultSet.getInt("id"));
					user.setLogin(resultSet.getString("login"));
					users.add(user);
				}

			} catch (Exception e) {
				throw new DBException(e.getMessage(), e.getCause());
			}

		}

		return users;
	}

	public boolean insertUser(User user) throws DBException {
		if (!user.getLogin().matches("\\w+") || user.getLogin().length() > 10) {
			throw new IllegalArgumentException("wrong login format");
		}

		String sql = "INSERT INTO users (login) VALUES (?)";
		try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, user.getLogin());
			statement.executeUpdate();

			ResultSet resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				user.setId(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			throw new DBException("insertUser() failed", e);
		}

		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		boolean result = true;
		try (
			 PreparedStatement statement = connection.prepareStatement("DELETE FROM users WHERE login=?")) {
			int k = 1;
			for (User u : users) {
				statement.setString(k, u.getLogin());
				result = result && statement.executeUpdate() > 0;
			}
		} catch (SQLException ex) {
			throw new DBException("DB deleteUsers() problem", ex);
		}
		return result;
	}

	public User getUser(String login) throws DBException {
		User user = new User();
		ResultSet resultSet;
		try {
			Statement statement = connection.createStatement();
			resultSet = statement.executeQuery("SELECT id, login FROM users WHERE login = '" + login + "'");
			resultSet.next();
			user.setId(resultSet.getInt("id"));

			user.setLogin(resultSet.getString("login"));
		} catch (Exception e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		ResultSet resultSet;
		try {
			Statement statement = connection.createStatement();
			resultSet = statement.executeQuery("SELECT id, name FROM teams WHERE name = '" + name + "'");
			resultSet.next();
			team.setId(resultSet.getInt("id"));

			team.setName(resultSet.getString("name"));
		} catch (Exception e) {
			throw new DBException(e.getMessage(), e.getCause());
		}

		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		ResultSet resultSet;

		if (connection != null) {
			try {
				Statement statement = connection.createStatement();
				resultSet = statement.executeQuery("SELECT * FROM teams");
				Team team;
				while (resultSet.next()) {
					team = new Team();
					team.setId(resultSet.getInt("id"));
					team.setName(resultSet.getString("name"));
					teams.add(team);
				}

			} catch (Exception e) {
				throw new DBException(e.getMessage(), e.getCause());
			}

		}

		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		String sql = "INSERT INTO teams (name) VALUES (?)";
		try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, team.getName());
			statement.executeUpdate();



			ResultSet resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				team.setId(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			throw new DBException("insertTeam() failed", e);
		}


		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		try {
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		String sql = "INSERT INTO users_teams (user_id, team_id) VALUES (?,?)";

		try (PreparedStatement statement = connection.prepareStatement(sql)) {
			for (Team team : teams) {
				statement.setInt(1, user.getId());
				statement.setInt(2, team.getId());

				statement.executeUpdate();
			}
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException ex) {
				throw new DBException("setTeamsForUser() failed", ex);
			}
			throw new DBException("setTeamsForUser() failed", e);
		}

		try {
			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		ResultSet resultSet;

		String sql = "SELECT id, name FROM teams WHERE id IN " +
				"(SELECT team_id FROM users_teams WHERE user_id = ?)";

		if (connection != null) {
			try {
				PreparedStatement statement = connection.prepareStatement(sql);
				statement.setInt(1, user.getId());
				resultSet = statement.executeQuery();
				Team team;
				while (resultSet.next()) {
					team = new Team();
					team.setId(resultSet.getInt("id"));
					team.setName(resultSet.getString("name"));

					teams.add(team);
				}

			} catch (Exception e) {
				throw new DBException("getUserTeams() failed", e);
			}

		}

		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		boolean result;
		try (
			 PreparedStatement statement =
					 connection.prepareStatement("DELETE FROM teams WHERE name=?")) {
			int k = 1;
			statement.setString(k, team.getName());
			result = statement.executeUpdate() > 0;
		} catch (SQLException ex) {
			throw new DBException("DB deleteTeam() problem", ex);
		}
		return result;
	}

	public boolean updateTeam(Team team) throws DBException {
		boolean result;

		try {
			PreparedStatement statement = connection.prepareStatement("UPDATE teams SET name=? WHERE id=?");

			int k = 1;
			statement.setString(k++, team.getName());
			statement.setInt(k, team.getId());

			result = statement.executeUpdate() > 0;
		} catch (SQLException ex) {
			throw new DBException("DB updateTeam() problem", ex);
		}
		return result;
	}

	public Properties loadProperties() {
		Properties properties = new Properties();
		try (InputStream inputStream = new FileInputStream("app.properties")) {
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return properties;
	}
}
