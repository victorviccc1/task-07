package com.epam.rd.java.basic.task7.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBException extends Exception {

	public DBException(String message, Throwable cause) {
		try {
			throw new SQLException("\n" + message + "\n", cause);
		} catch (SQLException e) {
//			System.out.println("\n");
			e.printStackTrace();
//			System.out.println("\n");
		}
	}

}
