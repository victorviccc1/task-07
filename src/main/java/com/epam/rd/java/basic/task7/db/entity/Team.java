package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class Team {

	private int id;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		Team team = new Team();
		team.setName(name);
//		team.setId(team.id);
		return team;
	}

	@Override
	public String toString() {
				return "" + name;

//		return "Team{" +
//				"id=" + id +
//				", name='" + name + '\'' +
//				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Team)) return false;
		Team team = (Team) o;
		return Objects.equals(getName(), team.getName());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getName());
	}

}
